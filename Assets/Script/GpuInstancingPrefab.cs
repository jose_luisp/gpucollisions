﻿using UnityEngine;
using System.Collections.Generic;


/// <summary>
/// 170-180 fps editor i3 antiguo. 1600 cubos con raycast en update
/// </summary>
public class GpuInstancingPrefab : MonoBehaviour
{
    const float BATCH_MAX_FLOAT = 1023f;
    const int BATCH_MAX = 1023;
    const float RAY_DISTANCE_MAX = 3f;


    [Header("Configuration")]
    [SerializeField] private GameObject prefab;
    [SerializeField] private Material meshMaterial;
    
    [SerializeField] private int width;
    [SerializeField] private int depth;
    [SerializeField] private int height;
    [SerializeField] private float spacing;


    private MeshFilter meshFilter;
    private Vector4[] colorArray;
    private Matrix4x4[] matrices;
    private MaterialPropertyBlock propertyBlock;
    private RaycastHit[] rayHitBuffer = new RaycastHit[1];

    /// <summary>
    /// 
    /// podriamos hacer un raycast contra la matrix4x4 matrices, pero para no liarlo mas
    /// guardamos la posicion, rotacion, escala en una clase CubeData y hacemos el raycast
    /// contra listCube
    /// </summary>
    private List<CubeData> listCube = new List<CubeData>();

    void Start()
    {
        meshFilter = prefab.GetComponent<MeshFilter>();

        InitData();



    }




    private void InitData()
    {
        Vector3 pos = new Vector3();
        Vector3 scale = new Vector3(1, 1, 1);

        int count = width * depth;
        matrices = new Matrix4x4[count];
        colorArray = new Vector4[count];
        propertyBlock = new MaterialPropertyBlock();
        
        Color[] preDefColors =
        {

            Color.blue,
            Color.black,
            Color.clear,
            Color.cyan,
            Color.green,
            Color.gray,
            Color.grey,
            Color.magenta,
            Color.red,
            Color.white,
            Color.yellow

        };

        for (int i = 0; i < width; ++i)
        {
            for (int j = 0; j < depth; ++j)
            {
                int idx = (i * depth) + j;

                matrices[idx] = Matrix4x4.identity;

                pos.x = i * spacing;
                pos.y = Random.Range(-height, height);
                pos.z = j * spacing;

                listCube.Add(new CubeData {
                    pos = pos,
                    scale = scale,
                    rot = Quaternion.identity
                    
                });

                
                matrices[idx].SetTRS(pos, Quaternion.identity, scale);
                

                //grupo de colores
                colorArray[idx] = preDefColors[idx % preDefColors.Length];


            }
        }

        Debug.Log("Cubes Instantiated=" + listCube.Count);



    }

   

    void Update()
    {
       

        UpdatePhysics();
        RenderBatches();


    }


    private void UpdatePhysics()
    {

        for(int i = 0; i < listCube.Count; ++i)
        {


            ////Debug.DrawRay(listCube[i].pos, Vector3.forward, Color.yellow, 10);
            if (Physics.RaycastNonAlloc(listCube[i].pos, Vector3.forward, rayHitBuffer, RAY_DISTANCE_MAX) > 0)
            {

                Debug.Log("golpeado");
            }

        }



    }




    private void RenderBatches()
    {
        int total = width * depth;
        int batches = (int)Mathf.Ceil(total / BATCH_MAX_FLOAT);

        for (int i = 0; i < batches; ++i)
        {
            int batchCount = Mathf.Min(BATCH_MAX, total - (BATCH_MAX * i));

            int start = Mathf.Max(0, (i - 1) * BATCH_MAX);

            Matrix4x4[] batchedMatrices = GetBatchedMatrices(start, batchCount);
            Vector4[] batchedColorArray = GetBatchedArray(start, batchCount);

            propertyBlock.SetVectorArray("_Color", batchedColorArray);


            Graphics.DrawMeshInstanced(meshFilter.sharedMesh, 0, meshMaterial, batchedMatrices, batchCount, propertyBlock);



        }



    }




    private Vector4[] GetBatchedArray(int offset, int batchCount)
    {
        Vector4[] batchedArray = new Vector4[batchCount];

        for (int i = 0; i < batchCount; ++i)
        {
            batchedArray[i] = colorArray[i + offset];
        }

        return batchedArray;


    }



    private Matrix4x4[] GetBatchedMatrices(int offset, int batchCount)
    {
        Matrix4x4[] batchedMatrices = new Matrix4x4[batchCount];

        for (int i = 0; i < batchCount; ++i)
        {
            batchedMatrices[i] = matrices[i + offset];
        }

        return batchedMatrices;



    }





}


public class CubeData
{
    public Vector3 pos;
    public Quaternion rot;
    public Vector3 scale;
   
    


}
