﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;



public class GpuInstancingParticles : MonoBehaviour
{
    const int BATCH_MAX = 1023;

    [Header("Configuration")]
    [SerializeField] public Mesh mesh;
    [SerializeField] public Material mat;
    [SerializeField] public Camera targetCamera;
    [SerializeField] public MeshRenderer maskRender;
    [Range(1, 60)][SerializeField] private int physicalUpdate = 10;

    [Header("Collider")]
    [SerializeField] public BoxCollider2D collider2d;
    [SerializeField] public Vector3 collider2ddata;

    [Header("RT")]
    [Range(0.25f, 1.0f)][SerializeField]public float rtSize = 1.0f;
    [SerializeField] public Rect m_generateRect = new Rect(0, 5, 15, 5);
    [Range(100f, -100f)][SerializeField] public float bottomOffset = 5f;
    [SerializeField] public ComputeShader particleComputeShader;
    

    

    private Vector4 generatePosPlayer = Vector4.zero;
    private float timedelta = 0;
    private RenderTexture particleRT;
    private CommandBuffer commandBuffer;
    private uint[] bufferArgs;
    private ComputeBuffer bufferWithArgs;
    private Vector4[] bufferPosition;
    private Vector4[] bufferParams;
    private ComputeBuffer computeBufferPosition;
    private ComputeBuffer computeBufferParams;
    private int particleKernelId;


    void Start()
    {
        bufferArgs = new uint[5] { mesh.GetIndexCount(0), BATCH_MAX, 0, 0, 0 };
        bufferWithArgs = new ComputeBuffer(1, sizeof(uint) * 5, ComputeBufferType.IndirectArguments);
        bufferWithArgs.SetData(bufferArgs);

        particleRT = new RenderTexture((int)(Screen.width * rtSize), (int)(Screen.height * rtSize), 0, RenderTextureFormat.ARGB32);

        if (maskRender != null)
        {
            maskRender.material.SetTexture("_ParticleRT", particleRT);
        }

        computeBufferParams = new ComputeBuffer(BATCH_MAX, sizeof(float) * 4);
        computeBufferPosition = new ComputeBuffer(BATCH_MAX, sizeof(float) * 4);

        bufferParams = new Vector4[BATCH_MAX];
        bufferPosition = new Vector4[BATCH_MAX];


        for (int i = 0; i < BATCH_MAX; i++)
        {
            Vector3 startpos = GeneratePos();

            bufferPosition[i].x = startpos.x;                 
            bufferPosition[i].y = startpos.y;                 
            bufferPosition[i].z = startpos.z;                 //posz
            bufferPosition[i].w = targetCamera.transform.position.y - bottomOffset;  //bottom pos

            bufferParams[i].x = 0.01f;                        //size x
            bufferParams[i].y = Random.Range(0.4f, 0.7f);     //size y
            bufferParams[i].z = Random.Range(4f, 6f);         //speed
            bufferParams[i].w = Time.time;                    //starttime



        }

        computeBufferPosition.SetData(bufferPosition);
        computeBufferParams.SetData(bufferParams);

        mat.SetBuffer("_cbufferPosition", computeBufferPosition);
        mat.SetBuffer("_cbufferParams", computeBufferParams);

        timedelta = Time.time;

        particleKernelId = particleComputeShader.FindKernel("CSParticleKernel");
        particleComputeShader.SetBuffer(particleKernelId, "_cbufferPosition", computeBufferPosition);
        particleComputeShader.SetBuffer(particleKernelId, "_cbufferParams", computeBufferParams);

        particleComputeShader.SetVector("_GenRect", new Vector4(m_generateRect.x, m_generateRect.y, m_generateRect.width, m_generateRect.height));

        //Process Collider
        {


            collider2ddata = collider2d.transform.position;
            collider2ddata.z = collider2d.transform.lossyScale.x * 0.5f;
            collider2ddata.y += collider2d.transform.lossyScale.y * 0.5f;

            particleComputeShader.SetVector("_collider", collider2ddata);



        }



    }



    private void Update()
    {
        float t = 1f / physicalUpdate;
        float curtime = Time.time;

        if ((curtime - timedelta) > t)
        {
            timedelta = curtime;
            
            
            //Process Collider
            {

                collider2ddata = collider2d.transform.position;
                collider2ddata.z = collider2d.transform.lossyScale.x * 0.5f;
                collider2ddata.y += collider2d.transform.lossyScale.y * 0.5f;

                particleComputeShader.SetVector("_collider", collider2ddata);

            }

            particleComputeShader.SetFloat("_Time", curtime);
            Vector4 cpos = targetCamera.transform.position;
            cpos.w = targetCamera.transform.position.y - bottomOffset;
            particleComputeShader.SetVector("_CameraPos", cpos);
            particleComputeShader.Dispatch(particleKernelId, BATCH_MAX / 64, 1, 1);

            if (commandBuffer == null)
            {

                commandBuffer = new CommandBuffer();
                var cameraevent = CameraEvent.BeforeForwardOpaque;
                commandBuffer.DrawMeshInstancedIndirect(mesh, 0, mat, 0, bufferWithArgs);

                commandBuffer.Blit(new RenderTargetIdentifier(BuiltinRenderTextureType.CameraTarget), particleRT);
                targetCamera.RemoveCommandBuffers(cameraevent);
                targetCamera.AddCommandBuffer(cameraevent, commandBuffer);




            }
        }
    }

    private void OnDisable()
    {


        if (particleRT != null) particleRT.Release();
        if (computeBufferParams != null) computeBufferParams.Release();
        if (computeBufferPosition != null) computeBufferPosition.Release();
        if (bufferWithArgs != null) bufferWithArgs.Release();



    }

    private void ReGenPos(ref Vector4 vec)
    {


        vec.x = generatePosPlayer.x + m_generateRect.width * (Random.value - 0.5f);
        vec.y = generatePosPlayer.y + m_generateRect.height * Random.value;
        vec.w = generatePosPlayer.z;



    }


    private Vector3 GeneratePos()
    {



        Vector3 vec = targetCamera.transform.position;
        vec.x += m_generateRect.x + m_generateRect.width * (Random.value - 0.5f);
        vec.y += m_generateRect.y + m_generateRect.height * Random.value;
        vec.z = 0;

        return vec;



    }





}
